DROP TABLE IF EXISTS association CASCADE;
DROP TABLE IF EXISTS event CASCADE;
DROP TABLE IF EXISTS user_ CASCADE;
DROP TABLE IF EXISTS intern CASCADE;
DROP TABLE IF EXISTS extern CASCADE;
DROP TABLE IF EXISTS member CASCADE;
DROP TABLE IF EXISTS staff_asso CASCADE;
DROP TABLE IF EXISTS staff_user CASCADE;
DROP TABLE IF EXISTS subscription CASCADE;
DROP TABLE IF EXISTS action CASCADE;
DROP TABLE IF EXISTS connexion CASCADE;
DROP TABLE IF EXISTS subscription_history CASCADE;
DROP TABLE IF EXISTS event_creation CASCADE;
DROP TABLE IF EXISTS asso_creation CASCADE;
DROP TABLE IF EXISTS server_log CASCADE;

/* WARNING : any modifications in those tables must be set in the corresponding
** history table below ! */

DROP TYPE IF EXISTS STATUS;
DROP TYPE IF EXISTS RANK;

CREATE TYPE STATUS AS ENUM ('attente', 'valide', 'refuse', 'cours', 'termine');
CREATE TYPE RANK   AS ENUM ('member', 'office', 'president');

CREATE TABLE association
(
  id              SERIAL,

  name            VARCHAR(64)  NOT NULL DEFAULT 'unnamed',
  url             VARCHAR(200) NOT NULL DEFAULT 'none',
  mail            VARCHAR(64)  NOT NULL DEFAULT 'none',
  image           VARCHAR(64)  NOT NULL DEFAULT '/img/assoNotFound.png',

  PRIMARY KEY(id)
);


CREATE TABLE event
(
  id              SERIAL,

  title           VARCHAR(64)  NOT NULL DEFAULT 'untitled',
  description     VARCHAR(300) NOT NULL DEFAULT 'none',
  begin_          TIMESTAMP    NOT NULL DEFAULT '2000-01-01 00:00:00',
  end_            TIMESTAMP    NOT NULL DEFAULT '2000-01-01 00:00:00',
  place           VARCHAR(200) NOT NULL DEFAULT 'none',
  creator_id      INTEGER      NOT NULL DEFAULT 0,
  association_id  INTEGER      NOT NULL DEFAULT 0,
  valid_pres      BOOLEAN      NOT NULL DEFAULT false,
  valid_resp      BOOLEAN      NOT NULL DEFAULT false,
  status          STATUS       NOT NULL DEFAULT 'attente', /* enum */
  close_subs      TIMESTAMP    NOT NULL DEFAULT '2000-01-01 00:00:00',
  nb_intern_max   INT          NOT NULL DEFAULT 0,
  nb_extern_max   INT          NOT NULL DEFAULT 0,
  price_intern    INT                   DEFAULT NULL, /* price_intern = NULL if unique price */
  price_extern    INT          NOT NULL DEFAULT 0,
  display_ticket  BOOLEAN      NOT NULL DEFAULT false,
  image           VARCHAR(100) NOT NULL DEFAULT '/img/eventNotFound.png',
  n_staff_tickets INTEGER      NOT NULL DEFAULT 0,
  premium         BOOLEAN      NOT NULL DEFAULT false,

  PRIMARY KEY(id)
);

CREATE TABLE user_
(
  id SERIAL,

  firstname       VARCHAR(64)  NOT NULL DEFAULT 'none',
  lastname        VARCHAR(64)  NOT NULL DEFAULT 'none',
  admin           BOOLEAN      NOT NULL DEFAULT false,
  responsible     BOOLEAN      NOT NULL DEFAULT false,
  intern          BOOLEAN      NOT NULL DEFAULT false,
  pwHash          VARCHAR(20)  NOT NULL DEFAULT 'none',
  mail            VARCHAR(100) NOT NULL DEFAULT 'none',

  PRIMARY KEY(id)
);

/*--------------------
 | Transition tables |
 ---------------------*/

CREATE TABLE member
(
  id SERIAL,

  user_id        INT  NOT NULL DEFAULT 0 REFERENCES user_,
  association_id INT  NOT NULL DEFAULT 0 REFERENCES association,
  rank           RANK NOT NULL DEFAULT 'member', /* enum */

  PRIMARY KEY(id)
);

CREATE TABLE staff_asso
(
  id SERIAL,

  association_id INT NOT NULL DEFAULT 0 REFERENCES association,
  event_id       INT NOT NULL DEFAULT 0 REFERENCES event,

  PRIMARY KEY(id)
);

CREATE TABLE staff_user
(
  id SERIAL,

  user_id  INT NOT NULL DEFAULT 0 REFERENCES user_,
  event_id INT NOT NULL DEFAULT 0 REFERENCES event,

  PRIMARY KEY(id)
);

CREATE TABLE subscription
(
  id SERIAL,

  user_id  INT     NOT NULL DEFAULT 0 REFERENCES user_,
  event_id INT     NOT NULL DEFAULT 0 REFERENCES event,
  goneOut  BOOLEAN NOT NULL DEFAULT false,
  used     BOOLEAN NOT NULL DEFAULT false,

  PRIMARY KEY(id)
);

/*-----------------
 | History tables |
 ------------------*/

CREATE TABLE action
(
  time TIMESTAMP NOT NULL DEFAULT '2000-01-01 00:00:00'
);

CREATE TABLE connexion
(
  user_id INTEGER NOT NULL DEFAULT 0 REFERENCES user_
) INHERITS (action);

CREATE TABLE subsciption_history
(
  user_id INTEGER NOT NULL DEFAULT 0 REFERENCES user_
) INHERITS (action);

CREATE TABLE event_creation
(
  event_id INTEGER NOT NULL DEFAULT 0 REFERENCES event
) INHERITS (action);

CREATE TABLE asso_creation
(
  asso_id INTEGER NOT NULL DEFAULT 0 REFERENCES association
) INHERITS (action);

CREATE TABLE server_log
(
  server_log VARCHAR(500) NOT NULL DEFAULT 'none'
) INHERITS (action);

/*------------------------
 | Change history tables |
 -------------------------*/

/* == Change history table ==
** Each table below has exactly the same fields as the ones above, except it has
** also a timestamp and a CHANGE field to keep track of what were inserted/deleted/changed
** and when. Notice that only the id field is not kept (it is overriden by the new serial
** id. */

DROP TYPE IF EXISTS CHANGE;
CREATE TYPE CHANGE AS ENUM ('inserted', 'deleted', 'changed');

CREATE TABLE association_history
(
  id              SERIAL,

  name            VARCHAR(64)  NOT NULL DEFAULT 'unnamed',
  url             VARCHAR(200) NOT NULL DEFAULT 'none',
  mail            VARCHAR(64)  NOT NULL DEFAULT 'none',
  image           VARCHAR(64)  NOT NULL DEFAULT '/img/assoNotFound.png',

  time            TIMESTAMP    NOT NULL DEFAULT '2000-01-01 00:00',
  change          CHANGE       NOT NULL DEFAULT 'inserted',

  PRIMARY KEY(id)
);


CREATE TABLE event_history
(
  id              SERIAL,

  title           VARCHAR(64)  NOT NULL DEFAULT 'untitled',
  description     VARCHAR(300) NOT NULL DEFAULT 'none',
  begin_          TIMESTAMP    NOT NULL DEFAULT '2000-01-01 00:00:00',
  end_            TIMESTAMP    NOT NULL DEFAULT '2000-01-01 00:00:00',
  place           VARCHAR(200) NOT NULL DEFAULT 'none',
  creator_id      INTEGER      NOT NULL DEFAULT 0,
  association_id  INTEGER      NOT NULL DEFAULT 0,
  valid_pres      BOOLEAN      NOT NULL DEFAULT false,
  valid_resp      BOOLEAN      NOT NULL DEFAULT false,
  status          STATUS       NOT NULL DEFAULT 'attente',
  close_subs      TIMESTAMP    NOT NULL DEFAULT '2000-01-01 00:00:00',
  nb_intern_max   INT          NOT NULL DEFAULT 0,
  nb_extern_max   INT          NOT NULL DEFAULT 0,
  price_intern    INT                   DEFAULT NULL,
  price_extern    INT          NOT NULL DEFAULT 0,
  display_ticket  BOOLEAN      NOT NULL DEFAULT false,
  image           VARCHAR(100) NOT NULL DEFAULT '/img/eventNotFound.png',
  n_staff_tickets INTEGER      NOT NULL DEFAULT 0,
  premium         BOOLEAN      NOT NULL DEFAULT false,

  time            TIMESTAMP    NOT NULL DEFAULT '2000-01-01 00:00',
  change          CHANGE       NOT NULL DEFAULT 'inserted',

  PRIMARY KEY(id)
);


CREATE TABLE user_history
(
  id SERIAL,

  firstname       VARCHAR(64)  NOT NULL DEFAULT 'none',
  lastname        VARCHAR(64)  NOT NULL DEFAULT 'none',
  admin           BOOLEAN      NOT NULL DEFAULT false,
  responsible     BOOLEAN      NOT NULL DEFAULT false,
  intern          BOOLEAN      NOT NULL DEFAULT false,
  pwHash          VARCHAR(20)  NOT NULL DEFAULT 'none',
  mail            VARCHAR(100) NOT NULL DEFAULT 'none',

  time            TIMESTAMP    NOT NULL DEFAULT '2000-01-01 00:00',
  change          CHANGE       NOT NULL DEFAULT 'inserted',

  PRIMARY KEY(id)
);


CREATE TABLE member_history
(
  id SERIAL,

  user_id        INT  NOT NULL DEFAULT 0 REFERENCES user_,
  association_id INT  NOT NULL DEFAULT 0 REFERENCES association,
  rank           RANK NOT NULL DEFAULT 'member', /* enum */

  time            TIMESTAMP    NOT NULL DEFAULT '2000-01-01 00:00',
  change          CHANGE       NOT NULL DEFAULT 'inserted',

  PRIMARY KEY(id)
);


CREATE TABLE staff_asso_history
(
  id SERIAL,

  association_id INT NOT NULL DEFAULT 0 REFERENCES association,
  event_id       INT NOT NULL DEFAULT 0 REFERENCES event,

  time            TIMESTAMP    NOT NULL DEFAULT '2000-01-01 00:00',
  change          CHANGE       NOT NULL DEFAULT 'inserted',

  PRIMARY KEY(id)
);


CREATE TABLE staff_user_history
(
  id SERIAL,

  user_id  INT NOT NULL DEFAULT 0 REFERENCES user_,
  event_id INT NOT NULL DEFAULT 0 REFERENCES event,

  time            TIMESTAMP    NOT NULL DEFAULT '2000-01-01 00:00',
  change          CHANGE       NOT NULL DEFAULT 'inserted',

  PRIMARY KEY(id)
);


CREATE TABLE subscription_history
(
  id SERIAL,

  user_id  INT     NOT NULL DEFAULT 0 REFERENCES user_,
  event_id INT     NOT NULL DEFAULT 0 REFERENCES event,
  goneOut  BOOLEAN NOT NULL DEFAULT false,
  used     BOOLEAN NOT NULL DEFAULT false,

  time            TIMESTAMP    NOT NULL DEFAULT '2000-01-01 00:00',
  change          CHANGE       NOT NULL DEFAULT 'inserted',

  PRIMARY KEY(id)
);
