package com.example.albanbuguet.tender_ticket_app

import android.Manifest
import android.app.Dialog
import android.app.usage.UsageEvents
import android.content.pm.PackageManager
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView
import java.net.URL

class ScanActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {

    private var mScannerView: ZXingScannerView? = null
    private val RECORD_REQUEST_CODE = 101
    private val TAG = "Access camera: "
    private lateinit var buttonEntrance : Button
    private lateinit var buttonExit : Button
    private lateinit var buttonCancel : Button
    private lateinit var ip : String
    private var fakeCounter = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mScannerView = ZXingScannerView(this)   // Programmatically initialize the scanner view
        setContentView(mScannerView)

        ip = intent.getStringExtra("IP")
        fakeCounter = 0;
        setupPermissions()

    }

    public override fun onResume() {
        super.onResume()
        mScannerView!!.setResultHandler(this) // Register ourselves as a handler for scan results.
        mScannerView!!.startCamera()          // Start camera on resume
    }

    public override fun onPause() {
        super.onPause()
        mScannerView!!.stopCamera()           // Stop camera on pause
    }

    override fun handleResult(rawResult: Result) {
        // Do something with the result here
        Log.v("tag", rawResult.text); // Prints scan results
        // Log.v("tag", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)

        var url = "$ip/${rawResult.text}"


        Thread({
            val result = URL("$url").readText()
            runOnUiThread({
                Toast.makeText(this, result, Toast.LENGTH_SHORT).show()
                if(result == "true"){
                    val dialog = Dialog(this)
                    dialog.setCanceledOnTouchOutside(false)
                    dialog.setContentView(R.layout.dialog_scan)
                    dialog.show()

                    //TODO: onbackpressed && on touch pressed
                    buttonEntrance = dialog.findViewById(R.id.btEntrance)
                    buttonEntrance.setOnClickListener({
                        dialog.hide()
                        Toast.makeText(this, "Entrance Clicked", Toast.LENGTH_SHORT).show()

                        Thread({
                            val result1 = URL("$ip/scanAppIO/1/3/IN/").readText()
                            runOnUiThread({
                                if(result1 == "true"){
                                    val dialog1 = Dialog(this)
                                    dialog1.setCanceledOnTouchOutside(false)
                                    dialog1.setContentView(R.layout.dialog_validation)
                                    var msg = dialog1.findViewById<TextView>(R.id.tvNameDialog2)
                                    msg.setTextColor(Color.GREEN)
                                    msg.text = "It's a success !"
                                    dialog1.show()

                                    var buttonOk = dialog1.findViewById<Button>(R.id.btOk)
                                    buttonOk.setOnClickListener({
                                        dialog1.hide()
                                        mScannerView?.resumeCameraPreview(this)
                                    })

                                }else{
                                    val dialog1 = Dialog(this)
                                    dialog.setCanceledOnTouchOutside(false)
                                    dialog1.setContentView(R.layout.dialog_validation)
                                    var msg = dialog1.findViewById<TextView>(R.id.tvNameDialog2)
                                    msg.setTextColor(Color.RED)
                                    msg.text = "Already IN !"
                                    dialog1.show()

                                    var buttonOk = dialog1.findViewById<Button>(R.id.btOk)
                                    buttonOk.setOnClickListener({
                                        dialog1.hide()
                                        mScannerView?.resumeCameraPreview(this)
                                    })

                                }
                            })
                        }).start()
                    })

                    buttonExit = dialog.findViewById(R.id.btExit)
                    buttonExit.setOnClickListener({
                        dialog.hide()
                        Toast.makeText(this, "Exit Clicked", Toast.LENGTH_SHORT).show()

                        Thread({
                            val result1 = URL("$ip/scanAppIO/1/3/OUT/").readText()
                            runOnUiThread({
                                if(result1 == "true"){
                                    val dialog1 = Dialog(this)
                                    dialog1.setCanceledOnTouchOutside(false)
                                    dialog1.setContentView(R.layout.dialog_validation)
                                    var msg = dialog1.findViewById<TextView>(R.id.tvNameDialog2)
                                    msg.setTextColor(Color.GREEN)
                                    msg.text = "It's a success !"

                                    dialog1.show()

                                    var buttonOk = dialog1.findViewById<Button>(R.id.btOk)
                                    buttonOk.setOnClickListener({
                                        dialog1.hide()
                                        mScannerView?.resumeCameraPreview(this)
                                    })

                                }else{
                                    val dialog1 = Dialog(this)
                                    dialog1.setCanceledOnTouchOutside(false)
                                    dialog1.setContentView(R.layout.dialog_validation)
                                    var msg = dialog1.findViewById<TextView>(R.id.tvNameDialog2)
                                    msg.setTextColor(Color.RED)
                                    msg.text = "Already OUT !"

                                    dialog1.show()

                                    var buttonOk = dialog1.findViewById<Button>(R.id.btOk)
                                    buttonOk.setOnClickListener({
                                        dialog1.hide()
                                        mScannerView?.resumeCameraPreview(this)
                                    })

                                }
                            })
                        }).start()

                    })

                    buttonCancel = dialog.findViewById(R.id.btCancel)
                    buttonCancel.setOnClickListener({
                        Toast.makeText(this, "Cancel Clicked", Toast.LENGTH_SHORT).show()
                        dialog.hide()
                        mScannerView?.resumeCameraPreview(this)
                    })
                } else {

                    val dialog1 = Dialog(this)
                    dialog1.setCanceledOnTouchOutside(false)
                    dialog1.setContentView(R.layout.dialog_validation)
                    var msg = dialog1.findViewById<TextView>(R.id.tvNameDialog2)
                    msg.setTextColor(Color.RED)
                    msg.text = "Wrong Ticket !"
                    dialog1.show()

                    var buttonOk = dialog1.findViewById<Button>(R.id.btOk)
                    buttonOk.setOnClickListener({
                        dialog1.hide()
                        mScannerView?.resumeCameraPreview(this)
                    })
                }
            })
        }).start()


        //MainActivity.tvresult!!.setText(rawResult.text)
        //onBackPressed()

        // If you would like to resume scanning, call this method below:
        //mScannerView?.resumeCameraPreview(this);
    }

    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Permission to camera denied")
            makeRequest()
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.CAMERA),
                RECORD_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            RECORD_REQUEST_CODE -> {

                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                    Log.i(TAG, "Permission has been denied by user")
                } else {
                    Log.i(TAG, "Permission has been granted by user")
                }
            }
        }

    }
}
