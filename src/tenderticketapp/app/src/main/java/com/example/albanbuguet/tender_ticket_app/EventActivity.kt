package com.example.albanbuguet.tender_ticket_app

import android.app.Dialog
import android.content.ComponentCallbacks2
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.beust.klaxon.string
import org.jetbrains.anko.custom.async
import org.jetbrains.anko.uiThread
import android.widget.Toast
import android.widget.Toast.*

import java.net.URL
import android.support.v4.widget.SwipeRefreshLayout
import android.widget.AdapterView.OnItemClickListener
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_event.*
import java.util.*


class EventActivity : AppCompatActivity() {

    private lateinit var  tab_events : ArrayList<String>
    private lateinit var  tab_eventsId : ArrayList<String>
    private lateinit var  tab_assos : ArrayList<String>
    private lateinit var mRandom: Random
    private lateinit var mHandler: Handler
    private lateinit var mRunnable:Runnable
    private lateinit var ip: String
    private lateinit var buttonOnline : Button
    private lateinit var buttonOffline : Button
    private lateinit var buttonCancel : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event)

        mHandler = Handler()

        loadActivity()


        swipe_refresh_layout.setOnRefreshListener {

            mRunnable = Runnable {
                loadActivity()
                swipe_refresh_layout.isRefreshing = false
            }

            // Execute the task after specified time
            mHandler.postDelayed(mRunnable, 2000)
        }

    }



    private fun loadActivity(){

        val Events : MyApplication = this.application as MyApplication
        tab_events = arrayListOf()
        tab_eventsId = arrayListOf()
        tab_assos =  arrayListOf()
        //mHandler = Handler()
        ip = "http://${Events.getIp()}:8000/ticket"



        val listView = findViewById<ListView>(R.id.event_list)

        Thread({
            //Do some Network Request
            var fail  = false
            var result1 = ""
            try {
                result1 = URL("$ip/listEventApp/").readText()
            } catch (e : Exception) {
                val intent = Intent(this, ModeActivity::class.java)
                fail = true

                startActivity(intent)
            }

            if(!fail) {


                runOnUiThread({
                    //Update UI
                    // Log.d("Request", result1)

                    val parser = Parser()
                    val stringBuilder = StringBuilder(result1)
                    val json = parser.parse(stringBuilder) as JsonArray<JsonObject>

                    for (i in 0..(json.size - 1)) {
                        val event = json[i]["fields"] as JsonObject
                        tab_events.add(event["title"].toString())
                        val asso = event["association_id"].toString()
                        tab_assos.add(asso)
                        val eventId = json[i]["pk"].toString()
                        tab_eventsId.add(eventId)
                        //Log.d("AssoId", asso)
                    }
                    //Log.d("TabEvent", tab_events.toString())

                    Thread({

                        var fail = false
                        for (i in 0..(tab_assos.size - 1)) {

                            var tmp = tab_assos[i]
                            Log.d("AssoId", tab_assos.toString())
                            tab_assos[i] = URL("$ip/getAssoNameApp/$tmp/").readText()

                        }
                        runOnUiThread({
                            //Log.d("TabAssos", tab_assos.toString())
                            /*if (fail){
                            Toast.makeText(this, "fail", Toast.LENGTH_SHORT).show()
                            tab_eventsId.clear()
                            tab_events.clear()
                            tab_assos.clear()
                            loadActivity()
                        }*/
                            listView.adapter = MyCustomAdapter(this, tab_events, tab_assos)
                        })

                    }).start()
                })
            }

        }).start()


        listView.onItemClickListener = OnItemClickListener { _, _, position, _ ->
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("EVENT_NAME", tab_events[position])
            intent.putExtra("EVENT_ID", tab_eventsId[position])
            intent.putExtra("ASSO_NAME", tab_assos[position])
            intent.putExtra("IP", ip)

            startActivity(intent)



        }


    }


    private class MyCustomAdapter(context: Context, tab_events: ArrayList<String>, tab_assos: ArrayList<String>): BaseAdapter() {

        private val mContext: Context
        private var  tab_events : ArrayList<String>
        private var  tab_assos : ArrayList<String>


        init {
            mContext = context
            this.tab_events = tab_events
            this.tab_assos = tab_assos
        }

        // responsible for how many rows in my list
        override fun getCount(): Int {
            return tab_events.size
        }

        // you can also ignore this
        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        // you can ignore this for now
        override fun getItem(position: Int): Any {
            return "TEST STRING"
        }

        // responsible for rendering out each row
        override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
            val layoutInflater = LayoutInflater.from(mContext)
            val rowEvent = layoutInflater.inflate(R.layout.row_event, viewGroup, false)
            if(position % 2 != 0)
                rowEvent.setBackgroundColor(Color.argb(150,210,210, 210 ))

            rowEvent.findViewById<TextView>(R.id.tvEvent).text = tab_events[position]
            rowEvent.findViewById<TextView>(R.id.tvAsso).text = "Organised by " + tab_assos[position]

            return rowEvent


        }

    }
}
