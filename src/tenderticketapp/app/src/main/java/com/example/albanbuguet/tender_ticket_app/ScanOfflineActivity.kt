package com.example.albanbuguet.tender_ticket_app

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView
import java.io.File
import java.net.URL

class ScanOfflineActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {

    private var mScannerView: ZXingScannerView? = null
    private val RECORD_REQUEST_CODE = 101
    private val TAG = "Access camera: "
    private lateinit var buttonEntrance : Button
    private lateinit var buttonExit : Button
    private lateinit var buttonCancel : Button
    private lateinit var eventId : String
    private lateinit var idsUsers : ArrayList<String>
    private lateinit var fileUserPresent : File


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mScannerView = ZXingScannerView(this)   // Programmatically initialize the scanner view

        eventId = intent.getStringExtra("EVENT_ID")

        setContentView(mScannerView)
        setupPermissions()

        val storageDir = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)
        var file = File(storageDir, "$eventId.json")
        var result1 = file.readText()

        val parser = Parser()
        val stringBuilder = StringBuilder(result1)
        val json = parser.parse(stringBuilder) as JsonArray<JsonObject>

        idsUsers = ArrayList()

        for (i in 0..(json.size-1)) {
            val user = json[i]["fields"] as JsonObject
            idsUsers.add(user["user"].toString())
        }

        Log.d("userpresent: " , idsUsers.toString() )

        fileUserPresent = File(storageDir, "${eventId}usersPresent.txt")
        if (!fileUserPresent.exists()){
            fileUserPresent.createNewFile()
        }




    }

    public override fun onResume() {
        super.onResume()
        mScannerView!!.setResultHandler(this) // Register ourselves as a handler for scan results.
        mScannerView!!.startCamera()          // Start camera on resume
    }

    public override fun onPause() {
        super.onPause()
        mScannerView!!.stopCamera()           // Stop camera on pause
    }

    override fun handleResult(rawResult: Result) {
        // Do something with the result here
        Log.v("tag", rawResult.text); // Prints scan results
        // Log.v("tag", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)

        var url = rawResult.text
        //scanAppVerif/<int:event_id>/<int:user_id>/

        Log.d("res", url)

        var goodTicket = true

        var event_id = ""
        var user_id = ""
        var i = 13
        try {
            while(true){
                if(url[i] != '/'){
                    event_id += url[i]
                    i++
                } else {
                    i++
                    break
                }
            }

            while (true){
                if(url[i] != '/'){
                    user_id += url[i]
                    i++
                } else {
                    break
                }
            }
        } catch (e : Exception) {
            goodTicket = false
        }





        if (goodTicket && event_id == eventId && idsUsers.contains(user_id)) {
            var presents = fileUserPresent.readText()

            val dialog = Dialog(this)
            dialog.setCanceledOnTouchOutside(false)
            dialog.setContentView(R.layout.dialog_scan)
            dialog.show()

            buttonEntrance = dialog.findViewById(R.id.btEntrance)
            buttonEntrance.setOnClickListener({
                dialog.hide()
                Toast.makeText(this, "Entrance Clicked", Toast.LENGTH_SHORT).show()

                if (!userIsIn(user_id, presents)) {
                    val dialog1 = Dialog(this)
                    dialog1.setCanceledOnTouchOutside(false)
                    dialog1.setContentView(R.layout.dialog_validation)
                    var msg = dialog1.findViewById<TextView>(R.id.tvNameDialog2)
                    msg.setTextColor(Color.GREEN)
                    msg.text = "It's a success !"
                    dialog1.show()

                    var buttonOk = dialog1.findViewById<Button>(R.id.btOk)
                    buttonOk.setOnClickListener({
                        dialog1.hide()
                        mScannerView?.resumeCameraPreview(this)
                    })

                    presents += user_id
                    fileUserPresent.delete()
                    fileUserPresent.createNewFile()
                    fileUserPresent.appendText(presents)

                } else {
                    val dialog1 = Dialog(this)
                    dialog1.setCanceledOnTouchOutside(false)
                    dialog1.setContentView(R.layout.dialog_validation)
                    var msg = dialog1.findViewById<TextView>(R.id.tvNameDialog2)
                    msg.setTextColor(Color.RED)
                    msg.text = "Already IN !"
                    dialog1.show()

                    var buttonOk = dialog1.findViewById<Button>(R.id.btOk)
                    buttonOk.setOnClickListener({
                        dialog1.hide()
                        mScannerView?.resumeCameraPreview(this)
                    })

                }
            })

            buttonExit = dialog.findViewById(R.id.btExit)
            buttonExit.setOnClickListener({
                dialog.hide()
                Toast.makeText(this, "Exit Clicked", Toast.LENGTH_SHORT).show()

                if (userIsIn(user_id, presents)) {
                    val dialog1 = Dialog(this)
                    dialog1.setCanceledOnTouchOutside(false)
                    dialog1.setContentView(R.layout.dialog_validation)
                    var msg = dialog1.findViewById<TextView>(R.id.tvNameDialog2)
                    msg.setTextColor(Color.GREEN)
                    msg.text = "It's a success !"

                    dialog1.show()

                    var buttonOk = dialog1.findViewById<Button>(R.id.btOk)
                    buttonOk.setOnClickListener({
                        dialog1.hide()
                        mScannerView?.resumeCameraPreview(this)
                    })

                    var newPresents = ""

                    for (i: Char in presents) {
                        if (i != user_id[0]) {
                            newPresents += i
                        }
                    }

                    fileUserPresent.delete()
                    fileUserPresent.createNewFile()
                    fileUserPresent.appendText(newPresents)

                } else {
                    val dialog1 = Dialog(this)
                    dialog1.setCanceledOnTouchOutside(false)
                    dialog1.setContentView(R.layout.dialog_validation)
                    var msg = dialog1.findViewById<TextView>(R.id.tvNameDialog2)
                    msg.setTextColor(Color.RED)
                    msg.text = "Already OUT !"

                    dialog1.show()

                    var buttonOk = dialog1.findViewById<Button>(R.id.btOk)
                    buttonOk.setOnClickListener({
                        dialog1.hide()
                        mScannerView?.resumeCameraPreview(this)
                    })

                }
            })

            buttonCancel = dialog.findViewById(R.id.btCancel)
            buttonCancel.setOnClickListener({
                Toast.makeText(this, "Cancel Clicked", Toast.LENGTH_SHORT).show()
                dialog.hide()
                mScannerView?.resumeCameraPreview(this)
            })

        } else {
            val dialog1 = Dialog(this)
            dialog1.setCanceledOnTouchOutside(false)
            dialog1.setCanceledOnTouchOutside(false)
            dialog1.setContentView(R.layout.dialog_validation)
            var msg = dialog1.findViewById<TextView>(R.id.tvNameDialog2)
            msg.setTextColor(Color.RED)
            msg.text = "Wrong Ticket !"
            dialog1.show()

            var buttonOk = dialog1.findViewById<Button>(R.id.btOk)
            buttonOk.setOnClickListener({
                dialog1.hide()
                mScannerView?.resumeCameraPreview(this)
            })
        }
    }


        //MainActivity.tvresult!!.setText(rawResult.text)
        //onBackPressed()

        // If you would like to resume scanning, call this method below:
        //mScannerView?.resumeCameraPreview(this);

    private fun userIsIn(id: String, userlist: String):Boolean {
        for(i : Char in userlist){
            if(i == id[0]){
                return true
            }
        }
        return false
    }


    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Permission to camera denied")
            makeRequest()
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.CAMERA),
                RECORD_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            RECORD_REQUEST_CODE -> {

                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                    Log.i(TAG, "Permission has been denied by user")
                } else {
                    Log.i(TAG, "Permission has been granted by user")
                }
            }
        }

    }
}
