package com.example.albanbuguet.tender_ticket_app

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import java.io.File

class ModeActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        val Events : MyApplication = this.application as MyApplication

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mode)

        val online = findViewById<Button>(R.id.btOnline)
        online.setOnClickListener({
            val intent = Intent(this, EventActivity::class.java)
            if(Events.getIp() == ""){
                Toast.makeText(this, "You need to set an IP first", Toast.LENGTH_SHORT).show()
            } else {
                startActivity(intent)
            }
        })

        val storageDir = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)

        val offline = findViewById<Button>(R.id.btOffline)
        offline.setOnClickListener({
            var file = File(storageDir, "ids.txt")
            var txt =file.readText()

            if (txt == "") {
                Toast.makeText(this, "You need to download data first", Toast.LENGTH_SHORT).show()

            } else {
                val intent = Intent(this, EventOfflineActivity::class.java)
                startActivity(intent)
            }
        })



        var file = File(storageDir, "ids.txt")
        if (!file.exists()){
            file.createNewFile()
        }

        var file2 = File(storageDir, "names.txt")
        if (!file2.exists()){
            file2.createNewFile()
        }

        var file3 = File(storageDir, "assos.txt")
        if (!file3.exists()){
            file3.createNewFile()
        }

        var file4 = File(storageDir, "ip.txt")
        if (!file4.exists()){
            file4.createNewFile()
        }

        var etIp = findViewById<EditText>(R.id.etIP)
        etIp.isCursorVisible = false
        etIp.setOnClickListener({
            etIp.isCursorVisible = true
        })
        var btValidate = findViewById<Button>(R.id.btValidate)

        btValidate.setOnClickListener({
            if(!etIp.text.isEmpty()){

                Events.setIp("${etIp.text}")

                etIp.isCursorVisible = false

                Toast.makeText(this, "IP changed", Toast.LENGTH_SHORT).show()
            }
        })







    }
}
