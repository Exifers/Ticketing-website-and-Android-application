package com.example.albanbuguet.tender_ticket_app

import android.app.Application
import android.os.Environment
import java.io.File

class MyApplication: Application() {

    private var tabEvent : ArrayList<String> = ArrayList()
    private var tabAsso : ArrayList<String> = ArrayList()
    private var tabId : ArrayList<String> = ArrayList()


    fun getIp() : String {
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)

        var file = File(storageDir, "ip.txt")
        return file.readText()
    }

    fun setIp(ip : String) {
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)

        var file = File(storageDir, "ip.txt")

        file.delete()
        file.createNewFile()
        file.appendText(ip)
    }

    fun getTabEvent() : ArrayList<String> {
        return tabEvent
    }

    fun getTabAsso() : ArrayList<String> {
        return tabAsso
    }
    fun getTabId() : ArrayList<String> {
        return tabId
    }

    fun addEvent(event: String){
        tabEvent.add(event)
    }

    fun addAsso(asso: String){
        tabAsso.add(asso)
    }

    fun addId(id: String){
        tabId.add(id)
    }

    fun addEventPos(event: String, i: Int){
        tabEvent[i] = event
    }

    fun addAssoPos(asso: String, i: Int){
        tabAsso[i] = asso
    }

    fun addIdPos(id: String, i: Int){
        tabId[i] = id
    }

    fun addEventToFile(id: String, name: String, asso:String){
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)


        var file = File(storageDir, "ids.txt")
        var ids = file.readText()
        for( i : Char in ids){
            if(i == id[0]){
                deleteEventFile(id, false)
                break
            }
        }

        file.appendText(id)
        var file2 = File(storageDir, "names.txt")
        file2.appendText(name + "\n")
        var file3 = File(storageDir, "assos.txt")
        file3.appendText(asso + "\n")
    }

    fun deleteEventFile(id: String, delJson: Boolean){
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)

        var file = File(storageDir, "ids.txt")

        var ids = file.readText()

        var newIds  = ""

        var count = 0
        var indice = 0

        for( i : Char in ids){
            if(i != id[0]){
                newIds += i
            } else {
                indice = count
            }
            count++

        }

        file.delete()
        file.createNewFile()
        file.appendText(newIds)

        var file2 = File(storageDir, "names.txt")
        var file3 = File(storageDir, "assos.txt")

        var names = file2.readText()
        var assos = file2.readText()

        var newnames = ""

        var cmpt = 0
        for(i : Char in names){
            if(cmpt != indice){
                newnames += i
            }
            if(i == '\n'){
                cmpt++
            }
        }

        var newassos = ""
        cmpt = 0

        for(i : Char in assos){
            if(cmpt != indice){
                newassos += i
            }
            if(i == '\n'){
                cmpt++
            }
        }
        file2.delete()
        file2.createNewFile()
        file2.appendText(newnames)

        file3.delete()
        file3.createNewFile()
        file3.appendText(newassos)

        var file4 = File(storageDir, id + ".json")
        if(delJson){
            file4.delete()
        }

        var fileUserPresent = File(storageDir, "${id}usersPresent.txt")
        if (fileUserPresent.exists()){
            fileUserPresent.delete()
        }


    }


}