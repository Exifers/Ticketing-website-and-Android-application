package com.example.albanbuguet.tender_ticket_app

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.text.Editable
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import java.io.File
import java.net.URL

class MainActivity : AppCompatActivity() {

    private lateinit var eventNameView : TextView
    private lateinit var messageView : TextView
    private lateinit var email : EditText
    private lateinit var password : EditText
    private lateinit var login : Button
    private lateinit var dlData : Button
    private lateinit var stScan : Button
    private lateinit var errorMsgView : TextView
    private lateinit var eventName : String
    private lateinit var assoName : String
    private lateinit var eventId : String
    private lateinit var ip: String




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        eventName = intent.getStringExtra("EVENT_NAME")
        assoName = intent.getStringExtra("ASSO_NAME")
        eventId = intent.getStringExtra("EVENT_ID")
        ip = intent.getStringExtra("IP")




        eventNameView = findViewById(R.id.tvEventTitle)
        eventNameView.text = eventName

        messageView = findViewById(R.id.tvMessage)
        messageView.text = "You have to be a staff member of $assoName to login"

        email = findViewById(R.id.etEmail)
        password = findViewById(R.id.etPassword)
        errorMsgView = findViewById(R.id.tvErrorMsg)
        errorMsgView.text = ""
        login = findViewById(R.id.btLogin)
        dlData = findViewById(R.id.btDownloadData)
        stScan = findViewById(R.id.btStartScanning)

        dlData.visibility = View.GONE
        stScan.visibility = View.GONE

        var logged = false



        login.setOnClickListener(View.OnClickListener {
            if(!logged) {
                if (!email.text.isEmpty() && !password.text.isEmpty()) {

                    Thread({
                        val result = URL("$ip/connectApp/$eventId/${email.text}/${password.text}/").readText()
                        runOnUiThread({
                            if (result == "true") {
                                errorMsgView.text = "You are logged"
                                errorMsgView.setTextColor(Color.GREEN)
                                logged = true

                                email.keyListener = null
                                password.keyListener = null
                                stScan.visibility = View.VISIBLE
                                dlData.visibility = View.VISIBLE


                            } else {
                                errorMsgView.text = "Wrong email or password"
                            }
                        })
                    }).start()
                } else {
                    errorMsgView.text = "You have to fill both fields"
                }
            }

            val view = this.currentFocus
            if(view != null){
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
        })

        stScan.setOnClickListener({
            val intent = Intent(this, ScanActivity::class.java)
            intent.putExtra("IP", ip)
            startActivity(intent)
        })

        dlData.setOnClickListener({
            val Events : MyApplication = this.application as MyApplication

            Events.addEvent(eventName)
            Events.addAsso(assoName)
            Events.addId(eventId)
            Thread({
                val result = URL("$ip/eventHorsLigne/$eventId/").readText()
                runOnUiThread {
                    val file = createFile(eventId, ".json")
                    file.createNewFile()
                    file.appendText(result)

                    Events.addEventToFile(eventId, eventName, assoName)

                    Toast.makeText(this, "Downloaded", Toast.LENGTH_SHORT).show()
                }
            }).start()

        })


    }

    private fun createFile(filename: String, suffix: String) : File {
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)
        if(!storageDir.exists()){
            storageDir.mkdir()
        }


        return File(storageDir, filename + suffix)
    }

}
