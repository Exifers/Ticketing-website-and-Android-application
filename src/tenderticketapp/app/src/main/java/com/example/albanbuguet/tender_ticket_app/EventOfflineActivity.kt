package com.example.albanbuguet.tender_ticket_app

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class EventOfflineActivity : AppCompatActivity() {

    private lateinit var tab_events: ArrayList<String>
    private lateinit var tab_eventsId: ArrayList<String>
    private lateinit var tab_assos: ArrayList<String>

    private lateinit var events: String
    private lateinit var eventsId: String
    private lateinit var assos: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        loadActivity()

    }

    private fun loadActivity() {

        setContentView(R.layout.activity_event_offline)
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)

        val Events: MyApplication = this.application as MyApplication

        var file = File(storageDir, "ids.txt")
        var file2 = File(storageDir, "names.txt")
        var file3 = File(storageDir, "assos.txt")

        eventsId = file.readText()
        events = file2.readText()
        assos = file3.readText()

        tab_eventsId = arrayListOf()

        var tmp = ""

        for (i: Char in eventsId) {
            tmp += i
            tab_eventsId.add(tmp)
            tmp = ""
        }

        val tmp_assos = assos.split("\n".toRegex())
        val tmp_events = events.split("\n".toRegex())

        tab_events = ArrayList(tmp_events)
        tab_assos = ArrayList(tmp_assos)

        Log.d("voici le tabassos", tab_assos.toString())
        Log.d("voici le tabevents", tab_events.toString())


        val listView = findViewById<ListView>(R.id.event_list2)
        listView.adapter = MyCustomAdapter(this, tab_events, tab_assos)

        listView.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val intent = Intent(this, ScanOfflineActivity::class.java)
            intent.putExtra("EVENT_ID", tab_eventsId[position])
            startActivity(intent)
        }

        listView.onItemLongClickListener = AdapterView.OnItemLongClickListener { parent, view, position, id ->
            val dialog = Dialog(this)
            dialog.setContentView(R.layout.dialog_delete)
            dialog.show()


            val buttonYes = dialog.findViewById<Button>(R.id.btYes)
            val buttonNo = dialog.findViewById<Button>(R.id.btNo)

            buttonNo.setOnClickListener({
                dialog.hide()
            })

            buttonYes.setOnClickListener({
                Events.deleteEventFile(tab_eventsId[position], true)
                loadActivity()
                dialog.hide()
            })
            true

        }
    }




    private class MyCustomAdapter(context: Context, tab_events: ArrayList<String>, tab_assos: ArrayList<String>): BaseAdapter() {

        private val mContext: Context
        private var  tab_events : ArrayList<String>
        private var  tab_assos : ArrayList<String>


        init {
            mContext = context
            this.tab_events = tab_events
            this.tab_assos = tab_assos
        }

        // responsible for how many rows in my list
        override fun getCount(): Int {
            return tab_events.size - 1
        }

        // you can also ignore this
        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        // you can ignore this for now
        override fun getItem(position: Int): Any {
            return "TEST STRING"
        }

        // responsible for rendering out each row
        override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
            val layoutInflater = LayoutInflater.from(mContext)
            val rowEvent = layoutInflater.inflate(R.layout.row_event, viewGroup, false)
            if(position % 2 != 0)
                rowEvent.setBackgroundColor(Color.argb(150,210,210, 210 ))

            rowEvent.findViewById<TextView>(R.id.tvEvent).text = tab_events[position]
            rowEvent.findViewById<TextView>(R.id.tvAsso).text = "Organised by " + tab_assos[position]

            return rowEvent
        }

    }
}
