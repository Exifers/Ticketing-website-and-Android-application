from ticket.models import User

u = User(
      firstname="admin",
      lastname="admin",
      admin=True,
      responsible=True,
      intern=True,
      pwhash="admin",
      mail="admin@epita.fr"
    )

u.save()

u = User(
      firstname="RA",
      lastname="RA",
      admin = False,
      responsible= True,
      intern = True,
      pwhash="RA",
      mail="RA@epita.fr"
    )
u.save()
