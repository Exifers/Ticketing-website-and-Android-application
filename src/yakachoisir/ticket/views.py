from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required


from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.core.mail import send_mail
from django import forms
from django.utils import timezone
from django.db.models import Q
from django.core import serializers
import smtplib, sys
import datetime as datet
import calendar as calen
import functools

from paypal.standard.forms import PayPalPaymentsForm

import random, pytz, datetime, qrcode

from .models import Action, AssoCreation, Association, Connexion, Event, EventCreation, Member, ServerLog, StaffUser, SubsciptionHistory, Subscription, User, PasswordRecovery
from django.conf import settings


import os.path as op
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import encoders

import fpdf
from fpdf import FPDF

#IP = "http://127.0.0.1:8000/ticket/"
IP = "http://10.0.2.15:8000/ticket/"

def addServerLog(string):
    log = ServerLog(time = timezone.now(), server_log = string)
    log.save()


def home(request):
    now = timezone.now()
    list_event = Event.objects.filter(begin_field__gte = now).order_by("begin_field")
    home_calendar = make_calendar(list_event)
    return render(request, 'ticket/index.html', { 'list_event': list_event, 'calendar': home_calendar, 'admin_mail' : get_admin_mail() })

def OK(request):
    return HttpResponse("GG tu geres!")

def KO(request):
    return render(request, 'ticket/invalid_password.html', {})

#================================= Connexion =================================

def connection_form(request):
    return render(request, 'ticket/connection.html')

def connection_DB(request):
    email = request.POST.get('mail', '')
    password = request.POST.get('password', '')
    try:
        u = User.objects.get(mail=email, pwhash = password, intern = False)
    except (User.DoesNotExist):
        try:
            u = User.objects.get(mail = email, admin = True)
        except (User.DoesNotExist):
            return HttpResponseRedirect(reverse('ticket:KO'))
    return HttpResponseRedirect(reverse('ticket:connected', args=(u.id,)))

def connected(request, user_id):
    now = timezone.now()
    list_event = Event.objects.filter(begin_field__gte = now).order_by("begin_field")
    
    addServerLog("User: " + str(user_id) + " new connection")
    return render(request, 'ticket/connected.html', { 'user_id': user_id , "list_event": list_event, "calendar": make_calendar(list_event), 'admin_mail': get_admin_mail()})

#=============================== Connexion_cri ===============================


def logged(request):
    user = request.user
    try:
        u = User.objects.get(mail = user.email, intern = True)
    except (User.DoesNotExist):
        u = User(firstname = user.first_name, lastname = user.last_name, admin = False, responsible = False, intern = True, pwhash = "", mail = user.email)
        u.save()
    addServerLog("User: " + str(u.id) + " new connection")
    return HttpResponseRedirect(reverse('ticket:connected', args=(u.id,)))

#================================= Inscription ================================

def inscription_form(request):
    return render(request, 'ticket/inscription_form.html')

def inscription(request):
    firstname_ = request.POST.get('firstname', "")
    lastname_ = request.POST.get('lastname', "")
    mail_ = request.POST.get('email', "")
    pw_ = request.POST.get('password', "")
    pw_c_ = request.POST.get('password_confirm',"" )

    if firstname_ == "":
        return render(request, 'ticket/inscription_form.html', { 'error':'prénom non renseigné' } )
    elif lastname_ == "":
        return render(request, 'ticket/inscription_form.html', { 'error':'nom non renseigné' } )
    elif mail_ == "":
        return render(request, 'ticket/inscription_form.html', { 'error':'email non renseigné' } )
    elif pw_ == "":
        return render(request, 'ticket/inscription_form.html', { 'error':'mot de passe non  renseigné' } )
    elif pw_c_ == "":
        return render(request, 'ticket/inscription_form.html', { 'error':'confirmation de mot de passe non renseignée' } )
    elif pw_ != pw_c_:
        return render(request, 'ticket/inscription_form.html', { 'error':'les mots de passe de correspondent pas' } )
    if (not "@" in list(mail_)):
        return render(request, 'ticket/inscription_form.html', { 'error':'adresse mail non valide' } )

    try:
        u = User.objects.get(mail=mail_)
        return HttpResponseRedirect(reverse('ticket:inscription_miss', args=('Error: This email address is already used',)))
    except (User.DoesNotExist):
        u = User(firstname = firstname_,
                 lastname = lastname_,
                 admin = False,
                 responsible = False,
                 intern = False,
                 pwhash = pw_,
                 mail = mail_)
        u.save()
        return HttpResponseRedirect(reverse('ticket:connected', args=(u.id,)))

def send_email_custom(to, obj, sub, path = None, filename = None):
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login("billeterie.epita@gmail.com", "epitaticket42")

    msg = MIMEMultipart()
    msg['Subject'] = sub
    msg['From'] = "billeterie@epita.com"
    msg['To'] = to
    msg['date'] = formatdate(localtime=True)

    msg.attach(MIMEText(obj))

    if path != None:
        f  = open(path, 'rb')

        part = MIMEBase('application', "octet-steam")
        with open(filename, 'rb') as file:
            part.set_payload(file.read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition',
                                        'attachment; filename="Ticket.pdf"'.format(op.basename(path)))
        msg.attach(part)

    txt = msg.as_string()

    server.sendmail("billeterie@epita.com", to, txt)
    server.quit()

def inscription_miss(request, error_message):
    return render(request, "ticket/error.html", {'string': error_message})

#================================ Reset Password ==============================

def createRandomInteger():
    rand = random.randint(0,  2147483647) # Min and max models.IntegerField
    try:
        field = PasswordRecovery.objects.get(token=rand)
        return createRandomInteger();
    except (PasswordRecovery.DoesNotExist):
        return rand

def recovery_password_form(request):
    return render(request, 'ticket/passwordRecovery_form.html')

def recovery_password(request):
    mail = request.POST.get('email', "")
    if mail == "":
        return HttpResponseRedirect(reverse('ticket:recovery_password_form'))

    uniqueRand = 0
    try:
        u = User.objects.get(mail=mail)
        uniqueRand = createRandomInteger()
        pw_reco = PasswordRecovery.objects.get(mail=mail)
        pw_reco.token = uniqueRand
        pw_reco.save()

    except (User.DoesNotExist):
        return HttpResponseRedirect(reverse('ticket:recovery_password_form'))
    except (PasswordRecovery.DoesNotExist):
        pw_reco = PasswordRecovery(mail = mail, token = uniqueRand)
        pw_reco.save()

    mail_subject = 'Vous avez demandé un changment de mot de passe.\nVeuillez vous rendre sur le lien suivant:\n\t' + IP + str(uniqueRand) + '/resetPassword_check/'

    send_email_custom(u.mail, mail_subject, "Reinitialise votre mot de passe")
    return render(request, "ticket/message.html", {'string': "Un mail a été envoyé à l'adresse " + u.mail, 'user_id': u.id, 'bool': True })

def reset_password_form(request, token):
    return render(request, 'ticket/reset_password_form.html', {'token': token})

def reset_password_check(request, token):
    p = get_object_or_404(PasswordRecovery, token = token)
    return HttpResponseRedirect(reverse('ticket:reset_password_form', kwargs={'token': token }))

def reset_password(request, token):
    pw = request.POST.get('password', "")
    pw_confirmation = request.POST.get('password_conf', "")

    if pw == "" or pw_confirmation == "" or pw != pw_confirmation:
        return HttpResponseRedirect('/ticket/' + str(token) + '/passwordRecovery/')

    try:
        p = PasswordRecovery.objects.get(token = token)
        u = User.objects.get(mail = p.mail)
        u.pwhash = pw
        p.delete()
        u.save()
        addServerLog("Mot de passe reinitialisé de l'utilisateur " + str(u.id))
        return HttpResponseRedirect(reverse('ticket:reset_password_success'))
    except (PasswordRecovery.DoesNotExist, User.DoesNotExist):
        return Http404('Error in database')

def reset_password_success(request):
    return render(request, "ticket/message.html", {'string': "Le mot de passe a bien été modifié", "bool": True})


#================================ Create Association ==========================

def create_association_check(request, user_id):
    if user_id == 0:
        return render(request, 'ticket:connection.html')
    u = get_object_or_404(User, id = user_id)
    if u.intern == False:
        return HttpResponseRedirect(request, 'ticket:connected', { 'user_id': user_id })
    return render(request, 'ticket/createAssociation_form.html', { 'user_id': user_id })

def create_association(request, user_id):
    n = request.POST.get('name', '')
    url = request.POST.get('url', '')
    mail = request.POST.get('mail', '')

    try:
        asso = Association.objects.get(name = n)
        return render(request, 'ticket/message.html', {'message':"Une association porte déjà ce nom", 'user_id':user_id})
    except (Association.DoesNotExist):
        asso = Association(name = n, url = url, mail = mail)
        asso.image = request.FILES['image']
        asso.save()

        u = get_object_or_404(User, id = user_id)
        m = Member(user = u, association = asso, rank = 'President')
        m.save()

        log = AssoCreation(time = timezone.now(), asso = asso)
        log.save()
        addServerLog("Nouvelle association " + str(asso.id))

        return render(request, 'ticket/message.html',  {'string': "L'association " + n + " a bien ete creee et vous en etes le president", "user_id": user_id})

def create_association_error(request, user_id):
    return render(request, "ticket/error.html", {'string': "Une association porte deja ce nom"})

def create_association_success(request, user_id):
    return render(request, 'ticket/message.html',  {'string': "L'association " + n + " a bien ete creee et vous en etes le president", "user_id": user_id})

#================================ Add member ==================================

def add_member_form(request, user_id, asso_id):
    u = get_object_or_404(User, id = user_id)
    a = get_object_or_404(Association, id = asso_id)

    m = get_object_or_404(Member, user = u, association = a)
    if m.rank != "President" and m.rank != "Bureau" and u.admin == False and u.responsible == False:
        return HttpResponseRedirect(reverse("ticket:KO"))
    return render(request, 'ticket/add_member_form.html', { 'user_id': user_id, 'asso_id': asso_id })

def add_member(request, user_id, asso_id):
    f_name = request.POST.get('firstname', '')
    l_name = request.POST.get('lastname', '')
    mail = request.POST.get('mail', '')

    try:
        u = User.objects.get(mail = mail)
        a = Association.objects.get(id = asso_id)

        m = Member(user = u, association = a, rank = "Member")
        m.save()

    except (User.DoesNotExist, Association.DoesNotExist):
        try:
            u = User.objects.get(firstane = f_name, lastname = l_name)
            m = Member(user = u, association = a, rank = "Member")
            m.save()
        except (User.DoesNotExist):
            return HttpResponseRedirect(reverse('ticket:add_member_form'), { 'user_id': user_id, 'asso_id': asso_id })
    return render(request, "ticket/message.html", {'string': "Un nouveau membre a rejoind l'association", 'user_id': user_id})
#================================ List member =================================

def list_member(request, user_id, asso_id):
    a = get_object_or_404(Association, id = asso_id)
    list = Member.objects.filter(association = a)
    return render(request, 'ticket/list_member.html', { 'user_id' : user_id, 'asso_id': asso_id, 'list': list })

#================================ Mes associations ============================

def my_association(request, user_id):
    u = get_object_or_404(User, id = user_id)
    list_asso = Member.objects.filter(user = u).order_by('association')

    b = False
    if u.admin == True or u.responsible == True:
        b = True

    return render(request, 'ticket/my_association.html', { 'user_id': user_id, 'list_asso': list_asso , 'new': b})

#================================ Mes evenements ==============================

def my_event(request, user_id):
    u = get_object_or_404(User, id = user_id)
    list_subs = Subscription.objects.filter(user = u).order_by("-event")
    list_event = [s.event for s in list_subs]
    return render(request, 'ticket/my_event.html', { 'user_id': user_id, 'list_event': list_event })
#================================ Evenements ==================================

def event(request):
    list_event = Event.objects.order_by("-begin_field")
    return render(request, 'ticket/event.html', { 'list_event': list_event })

def event_connected(request, user_id):
    u = get_object_or_404(User, id = user_id)
    list_event = Event.objects.order_by("-begin_field")
    user_events = []
    for e in list_event:
      try:
        Subscription.objects.get(user = u, event = e)
        user_events.append(e.id)
      except:
        pass
    return render(request, 'ticket/event_connected.html', { 'user_id': user_id, 'list_event': list_event, 'user_events': user_events})

#================================ Creation evenement ==========================

def can_create_event(user_id):
    return True

def create_event_form(request, user_id, asso_id):
    u = get_object_or_404(User, id = user_id)
    a = get_object_or_404(Association, id = asso_id)

    if can_create_event(user_id) == False:
        return HttpResponseRedirect(reverse("ticket:not_allowed"))

    return render(request, 'ticket/create_event_form.html', { 'user_id': user_id, 'asso_id': asso_id })

def requestToBoolean(b):
    return b == 'on'

def create_event(request, user_id, asso_id):

    utc = pytz.UTC

    title = request.POST.get("title", "")
    descr = request.POST.get("description", "")
    beg = utc.localize(datetime.datetime.strptime(request.POST.get("begin", ""), "%d/%m/%Y, %H:%M"))
    end = utc.localize(datetime.datetime.strptime(request.POST.get("end", ""), "%d/%m/%Y, %H:%M"))
    place = request.POST.get("place", "")
    valid_pres = requestToBoolean(request.POST.get("valid_pres", ""))
    valid_resp = requestToBoolean(request.POST.get("valid_resp", ""))
    status = request.POST.get("status", "")
    close = utc.localize(datetime.datetime.strptime(request.POST.get("close", ""), "%d/%m/%Y, %H:%M"))
    extern_max = request.POST.get("extern_max", "")
    intern_max = request.POST.get("intern_max", "")
    extern_price = request.POST.get("extern_price", "")
    intern_price = request.POST.get("intern_price", "")
    display = requestToBoolean(request.POST.get("display", ""))
    nb_staf = request.POST.get("nb_staff", "")
    prenium = requestToBoolean(request.POST.get("prenium", ""))

    now = timezone.now()

    if beg < now or end < now or close < now or end < beg:
        return render(request, "ticket/error.html", {'string': "La date ou l'heure est passée" })

    e = Event(title = title,
              description = descr,
              begin_field = beg,
              end_field = end,
              place = place,
              creator_id = user_id,
              association_id = asso_id,
              valid_pres = valid_pres,
              valid_resp = valid_resp,
              status = status,
              close_subs = close,
              nb_intern_max = intern_max,
              nb_extern_max = extern_max,
              price_intern = intern_price,
              price_extern = extern_price,
              display_ticket = display,
              n_staff_tickets = nb_staf,
              premium = prenium)
    e.image = request.FILES['image']
    e.save()

    log = EventCreation(time = timezone.now(), event = e)
    log.save()

    addServerLog("Nouvel event " + str(e.id))

    return render(request, "ticket/message.html", {'string': "Nouvel evenement crée", 'user_id': user_id})

#================================ Modification event =========================

def list_event_asso(request, user_id, asso_id):
    u = get_object_or_404(User, id = user_id)
    a = get_object_or_404(Association, id = asso_id)
    
    list_event = Event.objects.filter(association_id = asso_id)
    boolModif = False
    boolStaff = False
    boolDelete = False

    m = Member.objects.get(user = u, association = a)

    if m.rank == "President" or u.admin or u.responsible:
        boolModif = True
        boolStaff = True
        boolDelete = True
    elif m.rank == "Bureau":
        boolModif = False
        boolStaff = True

    return render(request, "ticket/list_event_asso.html", {'user_id': user_id, "asso_id": asso_id, "list_event": list_event, "boolModif": boolModif, "boolStaff": boolStaff, "boolDelete": boolDelete }) 

def modif_event_form(request, user_id, asso_id, event_id):
    e = get_object_or_404(Event, id = event_id)

    d = e.begin_field
    beg = str(d.day) + "/" + str(d.month) + "/" + str(d.year) + ", " + str(d.hour) + ":" + str(d.minute)

    d = e.end_field
    end = str(d.day) + "/" + str(d.month) + "/" + str(d.year) + ", " + str(d.hour) + ":" + str(d.minute)

    d = e.close_subs
    close = str(d.day) + "/" + str(d.month) + "/" + str(d.year) + ", " + str(d.hour) + ":" + str(d.minute)


    return render(request, 'ticket/modif_event_form.html', {"user_id": user_id, "asso_id": asso_id, "event_id": event_id, "e": e, "beg": beg, "end": end, "close": close})

def modif_event(request, user_id, asso_id, event_id):
    utc = pytz.UTC

    title = request.POST.get("title", "")
    descr = request.POST.get("description", "")
    beg = utc.localize(datetime.datetime.strptime(request.POST.get("begin", ""), "%d/%m/%Y, %H:%M"))
    end = utc.localize(datetime.datetime.strptime(request.POST.get("end", ""), "%d/%m/%Y, %H:%M"))
    place = request.POST.get("place", "")
    valid_pres = requestToBoolean(request.POST.get("valid_pres", ""))
    valid_resp = requestToBoolean(request.POST.get("valid_resp", ""))
    status = request.POST.get("status", "")
    close = utc.localize(datetime.datetime.strptime(request.POST.get("close", ""), "%d/%m/%Y, %H:%M"))
    extern_max = request.POST.get("extern_max", "")
    intern_max = request.POST.get("intern_max", "")
    extern_price = request.POST.get("extern_price", "")
    intern_price = request.POST.get("intern_price", "")
    display = requestToBoolean(request.POST.get("display", ""))
    nb_staf = request.POST.get("nb_staff", "")
    prenium = requestToBoolean(request.POST.get("prenium", ""))

    now = timezone.now()

    if beg < now or end < now or close < now or end < beg:
        return render(request, "ticket/error.html", {'string': "La date ou l'heure est passée" })

    e = get_object_or_404(Event, id = event_id)
    e.title = title
    e.begin_field = beg
    e.end_field = end
    e.place = place
    e.creator_id = user_id
    e.valid_pres = valid_pres
    e.valid_resp = valis_resp
    status = status
    e.close_subs = close
    e.nb_intern_max = intern_max
    e.nb_extern_max = extern_max
    e.price_extern = extern_price
    e.price_intern = intern_price
    e.display_ticket = display
    e.n_staff_tickets = nb_staff
    e.save()

    addServerLog("Modification event " + str(e.id))

    return render(request, "ticket/message.html", {'string': "L'evenement a ete modifie", 'user_id': user_id})
    return

def list_staff(request, user_id, asso_id, event_id):
    e = get_object_or_404(Event, id = event_id)
    list_staff = StaffUser.objects.filter(event = e)
    return render(request, "ticket/list_staff.html", {"list_staff": list_staff, "user_id": user_id})

def staff_event_form(request, user_id, asso_id, event_id):
    return render(request, "ticket/add_staff.html", {'user_id': user_id, "asso_id": asso_id, "event_id": event_id}) 

def staff_event(request, user_id, asso_id, event_id):
    f_name = request.POST.get('firstname', '')
    l_name = request.POST.get('lastname', '')
    mail = request.POST.get('mail', '')

    try:
        u = User.objects.get(mail = mail)
        a = Association.objects.get(id = asso_id)
        e = Event.objects.get(id = event_id)
        
        s = StaffUser(user = u, event = e)
        s.save()
        
    except (User.DoesNotExist, Association.DoesNotExist, Event.DoesNotExist):
        return render(request, "ticket/error.html", {'string': "Utilisateur inconnu"})
    
    return render(request, "ticket/message.html", {'string': "Un staff vient d'etre ajoute a l'evenement " + e.title, 'user_id': user_id})

def delete_event(request, user_id, asso_id, event_id):
    e = get_object_or_404(Event, id = event_id)
    e.delete()
    return render(request, "ticket/message.html", {"string": "Vous avez supprimez l'evenement", "user_id": user_id})

#================================ Inscription event ==========================

def inscription_event(request, user_id, event_id):
    u = get_object_or_404(User, id = user_id)
    e = get_object_or_404(Event, id = event_id)

    if (e.close_subs < timezone.now()):
      return render(request, 'ticket/message.html', {'message':"Les inscriptions sont fermées pour cet évènement", 'user_id':user_id})
 
    try:
        s = Subscription.objects.get(user = u, event = e)
        return render(request, 'ticket/inscription_done.html', {'success':False, 'user_id':user_id})
    except (Subscription.DoesNotExist):
        if (u.intern and e.price_intern == 0) or ((not u.intern) and e.price_extern == 0):
            return HttpResponseRedirect(reverse('ticket:inscription_success', args=(user_id, event_id)))
        
        price = 0
        if u.intern:
            price = e.price_intern
        else:
            price = e.price_extern
        
        name = "Inscription à l'evenement " + e.title

        paypal_dict = {
            "business": "billeterieEpita@gmail.com",
            "amount": price,
            "item_name": name,
            "invoice": "unique-invoice-id",
            "notify_url": IP + "IPN",
            "return": IP + str(user_id) + "/" + str(event_id) + "/inscriptionSuccess/",
            "cancel_return": IP + str(user_id) + "/" + str(event_id) + "/inscriptionFailed/",
        }

        form = PayPalPaymentsForm(initial=paypal_dict)
        context = {"form": form, "user_id": user_id, "event_id": event_id, "e":e}
        return render(request, "ticket/payment.html", context)

def createTicketPDF(qrcode, user, event):
    filename = 'ticket_' + event.title + '_' + str(user.id) + '.pdf'
    
    pdf = FPDF()
    pdf.add_page()
    
    pdf.set_font('Arial', 'B', 24)
    pdf.text(10, 20, 'Ticket')
    
    pdf.set_font('Arial', 'B', 12)
    pdf.text(20, 70, 'Propriétaire:')
    pdf.set_font('Arial', '', 12)
    pdf.text(50, 70, user.firstname + " " + user.lastname)
    
    pdf.set_font('Arial', 'B', 12)
    pdf.text(20, 80, 'Evenement:')
    pdf.set_font('Arial', '', 12)
    pdf.text(50, 80, event.title)

    pdf.set_font('Arial', 'B', 12)
    pdf.text(20, 90, 'Debut:')
    pdf.set_font('Arial', '', 12)
    pdf.text(50, 90, str(event.begin_field))

    pdf.set_font('Arial', 'B', 12)
    pdf.text(20, 100, 'Fin:')
    pdf.set_font('Arial', '', 12)
    pdf.text(50, 100, str(event.end_field))
    
    pdf.set_font('Arial', 'B', 12)
    pdf.text(20, 110, 'Lieu:')
    pdf.set_font('Arial', '', 12)
    pdf.text(50, 110, event.place)
    
    pdf.image(qrcode, 50, 150, 100, 100)
    
    pdf.output(filename, 'F')
    return filename

def inscription_success(request, user_id, event_id):
    u = get_object_or_404(User, id = user_id)
    e = get_object_or_404(Event, id = event_id)
    s = Subscription(user = u, event = e, goneout = False, used = False)
    s.save()
    
    qr = qrcode.QRCode(version = 1, error_correction = qrcode.constants.ERROR_CORRECT_H, box_size = 10, border = 4,)

    data = "scanAppVerif/" + str(event_id) + "/" + str(user_id) + "/"
    qr.add_data(data)
    qr.make(fit = True)
    img = qr.make_image()

    filename = "event" + str(event_id) + "_user" + str(user_id) + ".png"

    img.save(filename)

    to = u.mail
    obj = "Inscription à l'evenement " + e.title
    sub = "Merci de vous etre inscrit(e) à l'evenement " + e.title + ".\n\nVous trouverez en pièce jointe votre billet d'entrée"

    filename = createTicketPDF(filename, u, e)

    send_email_custom(to, sub, obj, filename, filename)

    log = SubsciptionHistory(time = timezone.now(), user = u)
    log.save()

    addServerLog("Inscription de l'utilisateur " + str(user_id) + " a l'event " + str(event_id))

    return render(request, "ticket/message.html", {"string": "Vous etes inscrit à l'evenement et recevrez votre billet en mail", "user_id": user_id})

def inscription_failed(request, user_id, event_id):
    return render(request, 'ticket/inscription_done.html', {'success':True, 'user_id':user_id})

#================================ Recheche ======== ==========================

def tmp(request, user_id):
    return render(request, 'ticket/tmp.html', { 'user_id': user_id })

def recherche (request):
    search = request.POST.get("search", "")

    list_event = Event.objects.filter(Q(title = search) | Q(place = search))
    list_asso = Association.objects.filter(Q(name = search) | Q(mail = search))

    return render(request, 'ticket/search_result.html', { 'list_event': list_event, 'list_asso': list_asso })

def recherche_connected (request, user_id):
    search = request.POST.get("search", "")

    list_event = Event.objects.filter(Q(title = search) | Q(place = search))
    list_asso = Association.objects.filter(name = search)

    return render(request, 'ticket/search_result_connected.html', { 'user_id': user_id, 'list_event': list_event, 'list_asso': list_asso })

#================================ Application ================================

def list_event_app(request):
    now = timezone.now()

    req = Event.objects.filter(begin_field__gte = datetime.datetime(now.year, now.month, now.day, 0, 0, 0, tzinfo = pytz.UTC), begin_field__lt = datetime.datetime(now.year, now.month, now.day + 1, 0, 0, 0, tzinfo = pytz.UTC)).order_by("begin_field")

    req = Event.objects.all()

    data = serializers.serialize("json", req)
    return HttpResponse(data)


def connectApp(request, event_id, mail, password):
    try:
        u = User.objects.get(mail = mail)
        e = Event.objects.get(id = event_id)
        if u.pwhash == "" or u.pwhash == password:
            s = StaffUser.objects.get(event = e, user = u)
            return HttpResponse("true")
        return HttpResponse("false")

    except (User.DoesNotExist, Event.DoesNotExist, StaffUser.DoesNotExist):
        return HttpResponse("false")

def scanAppVerif(request, event_id, user_id):
    u = get_object_or_404(User, id = user_id)
    e = get_object_or_404(Event, id = event_id)

    try:
        s = Subscription.objects.get(user = u, event = e)
        s.used = True
        s.save()
        return HttpResponse("true")

    except(Subscription.DoesNotExist):
        return HttpResponse("false")

def scanAppIO(request, event_id, user_id, stat):
    u = get_object_or_404(User, id = user_id)
    e = get_object_or_404(Event, id = event_id)

    try:
        s = Subscription.objects.get(user = u, event = e)

        if s.used == False:
            return HttpResponse("false")

        if stat == 'OUT':
            if s.goneout == True:
                s.goneout = False
                s.save()
                return HttpResponse("true")
            else:
                return HttpResponse("false")
        else:
            if s.goneout == False:
                s.goneout = True
                s.save()
                return HttpResponse("true")
            else:
                return HttpResponse("false")

    except(Subscription.DoesNotExist):
        return HttpResponse("false")

def eventHorsLigne(request, event_id):
    e = get_object_or_404(Event, id = event_id)

    try:
        s = Subscription.objects.filter(event = e)
        data = serializers.serialize("json", s)
        return HttpResponse(data)
    except (Subscription.DoesNotExist):
        return HttpResponse("ERROR")

def getAssoNameApp(request, asso_id):
    a = get_object_or_404(Association, id = asso_id)
    return HttpResponse(a.name)

##Calendar######################################################################

def make_calendar(events):
  """
    Generates a calendar for the current month, with events
  """
  # picking year and month at execution time
  year = datet.datetime.now().year
  month = datet.datetime.now().month 
  today = datet.datetime.now().day
  days = calen.monthcalendar(year, month)
  days = functools.reduce(lambda a,b : a+b, days)

  month_names = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", \
"Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"]

  home_calendar = {}
  home_calendar["year"] = year
  home_calendar["nextyear"] = year + 1 if month == 12 else year
  home_calendar["month"] = month_names[month]
  home_calendar["nextmonth"] = month_names[month + 1 if month < 12 else 1]
  home_calendar["days"] = []
  home_calendar["nextdays"] = []

  for d in days:
    if d == 0:
      home_calendar["days"].append({"number":"", "today":False, "Event":False})
      continue
    event_found = False
    for e in events:
      if (e.begin_field.year, e.begin_field.month, e.begin_field.day) \
== (year, month, d):
        home_calendar["days"].append({"number": d,
          "today":True if today == d else False,
          "event":True})
        event_found = True
        break
    if not event_found:
      home_calendar["days"].append({"number": d,
        "today":True if today == d else False,
        "event":False})

  nextyear = year + 1 if month == 12 else year
  nextmonth = month + 1 if month < 12 else 1
  nextdays = calen.monthcalendar(nextyear, nextmonth)
  nextdays = functools.reduce(lambda a,b : a+b, nextdays)

  for d in nextdays:
    if d == 0:
      home_calendar["nextdays"].append({"number":"", "today":False, "Event":False })
      continue
    event_found = False
    for e in events:
      if (e.begin_field.year, e.begin_field.month, e.begin_field.day) \
== (nextyear, nextmonth, d):
        home_calendar["nextdays"].append({"number": d,
          "today":False,
          "event":True})
        event_found = True
        break
    if not event_found:
      home_calendar["nextdays"].append({"number": d,
        "today":False,
        "event":False})
  return home_calendar

def get_admin_mail():
  mail = ""
  for line in open("ticket/static/adminmail.txt"):
    mail = line
    break
  return mail
