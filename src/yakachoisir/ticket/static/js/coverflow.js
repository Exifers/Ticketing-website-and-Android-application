
var ngApp = angular.module('ngApp', []);

ngApp.directive('coverflow', function(){
    return{
        restrict: 'E',
        scope: {
            list: '='
        },
        template:
        '<div class="coverflow">'+
            '<div class="coverflow__container">' +
                '<div class="coverflow__element" style="{{loadElementStyle($index)}}" ng-click="changeIndex($index)" ng-repeat="item in list">' +
                    '<h2  class="coverflow__title" style="text-align:center; margin-bottom:0;">{{ item.title }}</h2><h3 style="margin-top:0; color:grey; text-align:center;">{{ item.date_lieu }}</h3>' +
                    '<div class="coverflow__image">'+
                      '<img src="/ticket/media/{{ item.image }}" ></img>' +
                   '</div>' +
                '</div>' +
           '</div>' +
        '</div>',
        replace: true,
        link:function(scope, element, attrs)  {
                function listenToKeystrokes() {
                    var e;
                    $(document).keydown(function(e) {
                        if (e.which === 37) {
                            goLeft();
                        } else if (e.which === 39) {
                            goRight();
                        }
                        scope.$apply();
                    });
                }
                scope.coverflowItems = scope.list;
                function init() {
                    scope.index = parseInt(scope.coverflowItems.length / 2);
                    listenToKeystrokes();
                }
                init();
                function getNonFocussedElementStyle(loc, i, multiplier) {
                    return "transform: translateX(" + String(loc * 40 -12 * multiplier) + "%) rotateY(" + String(loc * -90) +"deg) scale(.6); z-index: " + String(loc * multiplier);
                }
                function getFocussedElementStyle(i) {
                    return "transform: translateZ(0);";
                }
                function goLeft() {
                    if(scope.index !== 0) {
                        scope.index--;
                    }
                }
                function goRight() {
                    if(scope.index !== scope.coverflowItems.length - 1) {
                        scope.index++;
                    }
                }
                scope.changeIndex = function(i) {
                    scope.index = i;
                };
                scope.loadElementStyle = function(i) {
                    var multiplier = scope.index - i;
                    if(i < scope.index) {
                       return getNonFocussedElementStyle(-1, i, multiplier);
                    } else if (i === scope.index) {
                       return getFocussedElementStyle(i);
                    } else {
                       return getNonFocussedElementStyle(1, i, multiplier);
                    }
                };
            }
        }
    }
);

ngApp.controller('dataController', ['$scope',
  function($scope) {
    allTitles = [];
    allTitlesData = document.getElementById('raw_data')
      .getElementsByClassName("title");
    for (var i = 0; i < allTitlesData.length; i++) {
      allTitles.push(allTitlesData[i].innerHTML);
    }
    allImages = [];
    allImagesData = document.getElementById('raw_data')
      .getElementsByClassName("imageEvent");
    for (var i = 0; i < allImagesData.length; i++) {
      allImages.push(allImagesData[i].innerHTML);
    }
    allPremium = [];
    allPremiumData = document.getElementById('raw_data')
      .getElementsByClassName("premium");
    for (var i = 0; i < allPremiumData.length; i++) {
      allPremium.push(allPremiumData[i].innerHTML);
    }
    allDateLieu = [];
    allDateLieuData = document.getElementById('raw_data')
      .getElementsByClassName("date_lieu");
    for (var i = 0; i < allDateLieuData.length; i++) {
      allDateLieu.push(allDateLieuData[i].innerHTML);
    }

    $scope.items = [];

    for (var i = 0; i < allTitles.length; i++) {
      if (allPremium[i] == "True") {
        $scope.items.push({
          title: allTitles[i] + " (premium)",
          image: allImages[i],
          date_lieu: allDateLieu[i]
        });
      }
    }

    for (var i = 0; i < allTitles.length; i++) {
      if (allPremium[i] == "False") {
        $scope.items.push({
          title: allTitles[i],
          image: allImages[i],
          date_lieu: allDateLieu[i]
        });
      }
    }
}]);
