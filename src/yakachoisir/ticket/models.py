# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Action(models.Model):
    time = models.DateTimeField()

class AssoCreation(models.Model):
    time = models.DateTimeField()
    asso = models.ForeignKey('Association', models.DO_NOTHING)

class Association(models.Model):
    name = models.CharField(max_length=64)
    url = models.CharField(max_length=200)
    mail = models.CharField(max_length=64)
    image = models.ImageField(upload_to = 'association_img/', blank = True)


class Connexion(models.Model):
    time = models.DateTimeField()
    user = models.ForeignKey('User', models.DO_NOTHING)

class Event(models.Model):
    ordering = ['premium', 'begin_field', 'title']
    title = models.CharField(max_length=64)
    description = models.CharField(max_length=300)
    begin_field = models.DateTimeField(db_column='begin_')  # Field renamed because it ended with '_'.
    end_field = models.DateTimeField(db_column='end_')  # Field renamed because it ended with '_'.
    place = models.CharField(max_length=200)
    creator_id = models.IntegerField()
    association_id = models.IntegerField()
    valid_pres = models.BooleanField()
    valid_resp = models.BooleanField()
    status = models.TextField()  # This field type is a guess.
    close_subs = models.DateTimeField()
    nb_intern_max = models.IntegerField()
    nb_extern_max = models.IntegerField()
    price_intern = models.IntegerField(blank=True, null=True)
    price_extern = models.IntegerField()
    display_ticket = models.BooleanField()
    image = models.ImageField(upload_to = 'event_img/', blank = True)
    n_staff_tickets = models.IntegerField()
    premium = models.BooleanField()

class EventCreation(models.Model):
    time = models.DateTimeField()
    event = models.ForeignKey(Event, models.DO_NOTHING)

class Member(models.Model):
    user = models.ForeignKey('User', models.DO_NOTHING)
    association = models.ForeignKey(Association, models.DO_NOTHING)
    rank = models.CharField(max_length=10)  # This field type is a guess.

class ServerLog(models.Model):
    time = models.DateTimeField()
    server_log = models.CharField(max_length=500)

class StaffAsso(models.Model):
    association = models.ForeignKey(Association, models.DO_NOTHING)
    event = models.ForeignKey(Event, models.DO_NOTHING)

class StaffUser(models.Model):
    user = models.ForeignKey('User', models.DO_NOTHING)
    event = models.ForeignKey(Event, models.DO_NOTHING)

class SubsciptionHistory(models.Model):
    time = models.DateTimeField()
    user = models.ForeignKey('User', models.DO_NOTHING)

class Subscription(models.Model):
    user = models.ForeignKey('User', models.DO_NOTHING)
    event = models.ForeignKey(Event, models.DO_NOTHING)
    goneout = models.BooleanField()
    used = models.BooleanField()

class User(models.Model):
    firstname = models.CharField(max_length=64)
    lastname = models.CharField(max_length=64)
    admin = models.BooleanField()
    responsible = models.BooleanField()
    intern = models.BooleanField()
    pwhash = models.CharField(max_length=20)
    mail = models.CharField(max_length=100)

class PasswordRecovery(models.Model):
    mail = models.CharField(max_length=100)
    token = models.IntegerField()
