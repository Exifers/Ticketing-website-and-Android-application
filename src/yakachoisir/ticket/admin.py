from django.contrib import admin

from .models import Association, Event, Member, ServerLog , User

admin.site.register(Association)
admin.site.register(Event)
admin.site.register(Member)
admin.site.register(ServerLog)
admin.site.register(User)
