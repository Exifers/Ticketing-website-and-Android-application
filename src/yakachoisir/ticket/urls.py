from django.urls import path

from django.conf.urls import url, include

from . import views

app_name = 'ticket'
urlpatterns = [
        path('', views.home, name='home'),
        path('OK/', views.OK, name='OK'),
        path('KO/', views.KO, name='KO'),

        path('connection_form/', views.connection_form, name='connection_form'),
        path('connection_DB/', views.connection_DB, name='connection_DB'),
        path('<int:user_id>/connected/', views.connected, name='connected'),

        url(r'^logged/$', views.logged, name='logged'),

        path('inscription/', views.inscription_form, name='inscription_form'),
        path('inscription_check/', views.inscription, name='inscription_check'),
        path('inscription_miss/', views.inscription_miss, name='inscription_miss'),


        path('passwordRecovery_form/', views.recovery_password_form, name='password_recovery_form'),
        path('passwordRecovery/', views.recovery_password, name='password_recovery'),
        path('<int:token>/resetPassword_check/', views.reset_password_check, name='reset_password_check'),
        path('<int:token>/resetPassword_form/', views.reset_password_form, name='reset_password_form'),
        path('<int:token>/resetPassword/', views.reset_password, name='reset_password'),
        path('resetPassword/success/', views.reset_password_success, name='reset_password_success'),

        path('<int:user_id>/createAssociation_check/', views.create_association_check, name = 'create_association_check'),
        path('<int:user_id>/createAssociation/', views.create_association, name = 'create_association'),
        path('<int:user_id>/createAssociation/success/', views.create_association_success, name = 'create_association_success'),
        path('<int:user_id>/createAssociation/error/', views.create_association_error, name = 'create_association_error'),


        path('<int:user_id>/<int:asso_id>/addMember_form/', views.add_member_form, name = 'add_member_form'),
        path('<int:user_id>/<int:asso_id>/addMember/', views.add_member, name = 'add_member'),
        path('<int:user_id>/<int:asso_id>/listMember/', views.list_member, name = 'list_member'),


        path('<int:user_id>/mesAssociations/', views.my_association, name = 'my_association'),
        path('<int:user_id>/mesEvenements/', views.my_event, name = 'my_event'),
        path('<int:user_id>/<int:asso_id>/createEvent_form/', views.create_event_form, name = 'create_event_form'),
        path('<int:user_id>/<int:asso_id>/createEvent/', views.create_event, name = 'create_event'),

        path('Evenements/', views.event, name = 'event'),
        path('<int:user_id>/Evenements/', views.event_connected, name = 'event_connected'),


        path('<int:user_id>/<int:event_id>/inscriptionEvenement/', views.inscription_event, name = 'inscription_event'),

        path('<int:user_id>/<int:event_id>/inscriptionFailed/', views.inscription_failed, name = 'inscription_failed'),

        path('<int:user_id>/<int:event_id>/inscriptionSuccess/', views.inscription_success, name = 'inscription_success'),
        
        
        path('<int:user_id>/tmp/', views.tmp, name = 'tmp'),
        path('<int:user_id>/resultatRecherche/', views.recherche_connected, name = "recherche_connected"),
        path('resultatRecherche/', views.recherche_connected, name = "recherche_connected"),


        path('listEventApp/', views.list_event_app, name = "list_event_app"),
        path('connectApp/<int:event_id>/<mail>/<password>/', views.connectApp, name = "connectApp"),
        path('scanAppVerif/<int:event_id>/<int:user_id>/', views.scanAppVerif, name = 'scanAppVerif'),
        path('scanAppIO/<int:event_id>/<int:user_id>/<stat>/', views.scanAppIO, name = 'scanAppIO'), #stat -> IN || OUT
        path('getAssoNameApp/<int:asso_id>/', views.getAssoNameApp, name = 'getAssoNameApp'),

        path('eventHorsLigne/<int:event_id>/', views.eventHorsLigne, name = 'eventHorsLigne'),


        url(r'^paypal/', include('paypal.standard.ipn.urls')),


        path('<int:user_id>/<int:asso_id>/listEvent/', views.list_event_asso, name = "list_event_asso"),

        path('<int:user_id>/<int:asso_id>/<int:event_id>/modif/', views.modif_event, name = "modif_event"),
        path('<int:user_id>/<int:asso_id>/<int:event_id>/modifForm/', views.modif_event_form, name = "modif_event_form"),
        path('<int:user_id>/<int:asso_id>/<int:event_id>/staff/', views.staff_event, name = "staff_event"),
        path('<int:user_id>/<int:asso_id>/<int:event_id>/staffForm/', views.staff_event_form, name = "staff_event_form"),
        path('<int:user_id>/<int:asso_id>/<int:event_id>/delete/', views.delete_event, name = "delete_event"),
        path('<int:user_id>/<int:asso_id>/<int:event_id>/list_staff/', views.list_staff, name = "list_staff"),
        ]
